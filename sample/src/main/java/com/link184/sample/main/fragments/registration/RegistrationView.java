package com.link184.sample.main.fragments.registration;

import android.support.v4.app.Fragment;

interface RegistrationView {
    Fragment getFragment();
}
