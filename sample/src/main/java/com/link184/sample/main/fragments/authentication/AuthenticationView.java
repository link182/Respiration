package com.link184.sample.main.fragments.authentication;

import android.support.v4.app.Fragment;

interface AuthenticationView {
    Fragment getFragment();
}
